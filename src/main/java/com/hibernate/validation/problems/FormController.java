package com.hibernate.validation.problems;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 */
@RestController
@RequestMapping("/form")
public class FormController {

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public List<FieldError> submitForm(@RequestBody @Valid Form form, BindingResult result) {

        if(result.hasErrors()) {
            return result.getFieldErrors();
        }

        return null;
    }

}
