package com.hibernate.validation.problems;

import org.hibernate.validator.constraints.*;

public class Form {
	
    @Length(max=10)
	public String name;

    @Email
	public String email;
	
//	@Range(min=18)
//	public Integer age;

//    @CreditCardNumber
//    public String creditCard;
}
